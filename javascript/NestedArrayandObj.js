let x = "";
const myObj = {
    name: "rehan",
    age: 30,
    cars: [
        { name: "lambo", models: ["Tron2020", "riderX", "bbt"] },
        { name: "ferrari", models: ["M320", "MX3", "MX5"] },
        { name: "Fiat", models: ["500", "Panda"] }
    ]
}

for (let i in myObj.cars) {
    x += "<h2>" + myObj.cars[i].name + "</h2>";
    for (let j in myObj.cars[i].models) {
        x += myObj.cars[i].models[j] + "<br>";
    }
}

document.getElementById("demo").innerHTML = x;