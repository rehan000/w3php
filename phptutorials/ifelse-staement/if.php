 <!-- if statement executes if one condition is true.  -->
 <?php
    // Syntax
    // if (condition) {
    //   code to be executed if condition is true;
    // }

        $time = date("H");

        if ($time < "24") {
            echo "Nice";
        }
 ?>