// Getters allow you to get properties via methods
var person = {
    firstName: "rehan",
    lastName: "fazal",
    language: "en",
    get lang() {
        return this.language;
    }
};

document.getElementById("demo").innerHTML = person.lang;