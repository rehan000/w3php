<?php
    //local scope -> can only be accessed within the function
    function myTest(){
        $y = 12; // local scope
        echo "<p>variable y inside a finction = $y</p>";
    }
    myTest();
    //using y ouside of function genrate error
    echo "<p>variable y outside a function = $y</p>";
?>