<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mysql_db";
try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $stmt = $conn->prepare("SELECT orders.orderid, students.fullname, orders.orderdate
  FROM orders
  INNER JOIN students ON orders.id=students.id");
  $stmt->execute();
  $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
  print_r($result);
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;
?>