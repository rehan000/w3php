<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "join";
try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $stmt = $conn->prepare("SELECT orderid, quantity,
  CASE
      WHEN quantity > 30 THEN 'The quantity is greater than 30'
      WHEN quantity = 30 THEN 'The quantity is 30'
      ELSE 'The quantity is under 30'
  END AS quantitytext
  FROM ordersdetails");
  $stmt->execute();
  $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
  print_r($result);
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;
?>