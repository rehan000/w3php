<!DOCTYPE HTML>  
<html>
<head>
</head>
<body>  

<?php
//define variables and set to empty values
$name = $email = $website = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $name = test_input($_POST["name"]);
  $email = test_input($_POST["email"]);
  $website = test_input($_POST["website"]);
}

function test_input($data) {
// trim removes both side of string of a word
  $data = trim($data); 

//stripslashes remove back slashes
  $data = stripslashes($data);

//htmlspecialchars Convert the predefined characters "<" and ">" to HTML entities
  $data = htmlspecialchars($data); 
  return $data;
}
?>

<h2>PHP Form Validation Example</h2>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">  
  Name: <input type="text" name="name">
  <br><br>
  E-mail: <input type="text" name="email">
  <br><br>
  Website: <input type="text" name="website">
  <br><br>
  <input type="submit" name="submit" value="Submit">  
</form>

<?php
echo "<h2>Your Input:</h2>";
echo $name;
echo "<br>";
echo $email;
echo "<br>";
echo $website;
?>

</body>
</html>