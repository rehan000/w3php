<!-- js assignment operator -->

<!-- Operator	Example	    Same As
    =	    x = y	    x = y
    +=	    x += y	    x = x + y
    -=	    x -= y	    x = x - y
    *=	    x *= y	    x = x * y
    /=  	x /= y	    x = x / y
    %=	    x %= y	    x = x % y
    **=	    x **= y	    x = x ** y -->
<html>
<body>

<h2>The += Operator</h2>

<p id="demo"></p>

<script>
var x = 5;
x += 5;
document.getElementById("demo").innerHTML = x;
</script>

</body>
</html>