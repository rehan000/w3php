<!-- The require statement is also used to include a file into the PHP code.
However, there is one big difference between include and require;
when a file is included with the include statement and PHP cannot find it, the script will continue to execute:
1.(require) will produce a fatal error (E_COMPILE_ERROR) and stop the script
2.(include) will only produce a warning (E_WARNING) and the script will continue -->
<html>
<body>

<div class="menu">
<?php require 'menu.php';?>

</div>

<h1>Welcome to my home page!</h1>
<p>acess the menu file whithing this page</p>
<p>it acess its all functionality</p>
<?php include 'footer.php';?>
</body>
</html>