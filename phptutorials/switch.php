<!-- switch statement to select one of many blocks of code to be executed -->
<?php
    // Syntax
    // switch (n) {
    //   case label1:
    //     code to be executed if n=label1;
    //     break;
    //   case label2:
    //     code to be executed if n=label2;
    //     break;
    //   case label3:
    //     code to be executed if n=label3;
    //     break;
    //     ...
    //   default:
    //     code to be executed if n is different from all labels;
    // }

    $favcar = "BMW";

    switch ($favcar) {
        case 'WV':
            echo "My favorite car is WV";
            break;
        case 'Benz':
            echo "My favorite car is Benz";
            break;
        case 'Ferari':
            echo "My favorite car is Ferari";
            break;
        case 'Audi':
            echo "My favorite car is Audi";
            break;
        case 'BMW':
            echo "My favorite car is BMW";
            break;
        
        default:
            echo "your favorite car name is not listed here ";
    }
?>