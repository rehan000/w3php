<!--  if...else Statement -->
<?php
    // Syntax
    // if (condition) {
    // code execute if condition is true
    // } else {
    // code executed if condition is false
    // }

    $time = date("H");
    if ($time < 20) {
        echo "very good";
    } else {
        echo "okkk";
    }
    
?>
