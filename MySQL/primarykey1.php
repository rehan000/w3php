<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mysql_db";

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $sql = "CREATE TABLE hero (
    id int NOT NULL,
    hollywood varchar(255) NOT NULL,
    bollywood varchar(255),
    age int,
    CONSTRAINT heros PRIMARY KEY (id,hollywood)
)";
  $conn->exec($sql);
    echo "table created with multiple primary-key constraints";
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;
?>