<!-- Spaces and line breaks are not important. An object definition can span multiple lines -->
<html>
<body>

<h2>js Objects</h2>

<p id="demo"></p>

<script>
var person = {
  firstName: "rehan",
  lastName: "fazal",
  age: 24,
  eyeColor: "brown"
};
document.getElementById("demo").innerHTML =
person.firstName + " is " + person.age + " years old.";
</script>

</body>
</html>