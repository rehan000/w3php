<?php
namespace Html;
class Table {
  public $title = "";
  public $numRows = 0;
  public function message() {
    echo "<p>Table '{$this->title}' has {$this->numRows} rows.</p>";
  }
}
$table = new Table();
$table->title = "My table";
$table->numRows = 5;
?>
<!-- Syntax
Declare a namespace called Html:

namespace Html; -->
<!DOCTYPE html>
<html>

<body>

    <?php
$table->message();
?>

</body>

</html>