<!-- Syntax
ALTER TABLE table_name
DROP COLUMN column_name;
for deleting coloum -->

<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "join";

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $sql = "ALTER TABLE customers
  DROP COLUMN email";
  $conn->exec($sql);
    echo "coloum deleted successfully";
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;
?>