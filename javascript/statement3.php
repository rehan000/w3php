<html>
<body>

<p>When separated by semicolons, multiple statements on one line are allowed.</p>
<!-- js ignore multiple white spaces -->
<p id="demo1"></p>

<script>
var a, b, c;
a = 5; b = 6; c = a + b;
document.getElementById("demo1").innerHTML = c;
</script>

</body>
</html>