<!-- To call a static property from a child class, use the parent keyword inside the child class: -->
<?php
class pi {
  public static $value=3.14;
}

class A extends pi {
  public function AStatic() {
    return parent::$value;
  }
}

echo A::$value;
echo "<br>";
$A = new A();
echo $A->AStatic();
?>