var x = myFunction(4, 3);

function myFunction(a, b) {
    return a * b;
}

(function() {
    document.getElementById("demo").innerHTML = "Hello! I called myself";
})();

function Op(z, x) {
    return arguments.length;
}

function string(x, y) {
    return x * y;
}

function param(c, d) {
    if (d === undefined) {
        d = 2;
    }
    return c * d;
}

function findMax() {
    let max = -Infinity;
    for (let i = 0; i < arguments.length; i++) {
        if (arguments[i] > max) {
            max = arguments[i];
        }
    }
    return max;
}

function add() {
    let counter = 0;

    function plus() { counter += 1; }
    plus();
    return counter;
}