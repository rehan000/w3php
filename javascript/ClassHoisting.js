//You cannot use the class yet.
//myCar = new Car("ferrari")
//This would raise an error.

class Car {
    constructor(brand) {
        this.carname = brand;
    }
}

//Now you can use the class:
let myCar = new Car("ferrari")