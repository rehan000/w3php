<!-- JavaScript does not have any print object or print methods.
    Only exception is that you can call the window.print() 
    method in the browser to print the content of the current window
 -->
<html>
<body>

<button onclick="window.print()">Print this page</button>

</body>
</html>