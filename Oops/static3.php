<!-- a static method from a child class, use the parent keyword inside the child class.
 Here, the static method can be public or protected. -->
<?php
class domain {
  protected static function getWebName() {
    return "google.com";
  }
}

class domainggl extends domain {
  public $websiteName;
  public function __construct() {
    $this->websiteName = parent::getWebName();
  }
}

$domainggl = new domainggl;
echo $domainggl -> websiteName;
?>