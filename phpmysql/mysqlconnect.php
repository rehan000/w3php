<?php
// PDO
    $servername = "localhost";
    $username = "root";
    $password = "";

    try {
        $conn = new PDO("mysql:host=$servername;dbname=mydb" ,$username,$password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        echo "connected successfully";
    } catch (PDOException $e) {
        echo "connrction failed" . $e->getMessage();
    }
    // The connection will be closed automatically when the script ends
    $conn = null;
?>