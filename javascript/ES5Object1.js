var person = {
        firstName: "ravi",
        lastName: "singh",
        language: "EN"
    }
    // Change Property
Object.defineProperty(person, "language", { enumerable: false });
// Display Properties
document.getElementById("demo").innerHTML = Object.getOwnPropertyNames(person);