<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mysql_db";

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $sql = "CREATE TABLE police (
    id int NOT NULL,
    pname varchar(255) NOT NULL,
    post varchar(255),
    age int,
    city varchar(255),
    CONSTRAINT CHK_Person CHECK (age>=18 AND city='delhi')
)";
  $conn->exec($sql);
    echo "table created with multiple CHECK constraints";
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;
?>