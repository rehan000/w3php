<?php 
    declare(strict_types=1); // strict requirement
    function setHeight(int $minheight = 50) {
    echo "The height is : $minheight <br>";
    }

    setHeight(350);
    setHeight(); // will use the default value of 50
    setHeight(135);
    setHeight(80);

    echo "<h2>Another Example</h2>";

    function mult(int $x, int $y) {
        $z = $x * $y;
        // To let a function return a value, use the return statement
        return $z;
      }
      
      echo "5 * 10 = " . mult(5, 10) . "<br>";
      echo "7 * 13 = " . mult(7, 13) . "<br>";
      echo "2 * 4 = " . mult(2, 4) . "<br>";

      echo "<h2>Another Example</h2>";

      function addNumbers(float $a, float $b) : float {
        //   To declare a type for the function return, add a colon ( : )
        //  and the type right before the opening curly ( { )bracket when declaring the function
        return $a + $b;
      }
      echo addNumbers(2.2, 5.8);

      echo "<h2>Another Example</h2>";

      function add(float $a, float $b) : int {
        //   You can specify a different return type, than the argument types,
        //  but make sure the return is the correct type
        return (int)($a + $b);
      }
      echo add(1.2, 5.2);

      echo "<h2>Another Example</h2>";

      function add_five(&$value) {
        //   Use a pass-by-reference argument to update a variable
        $value += 5;
      }
      
      $num = 2;
      add_five($num);
      echo $num;
?>