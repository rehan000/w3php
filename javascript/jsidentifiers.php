<!-- Identifiers are names.

In JavaScript, identifiers are used to name variables (and keywords, and functions, and labels).
The rules for legal names are much the same in most programming languages.
In JavaScript, the first character must be a letter, or an underscore (_), or a dollar sign ($).
Subsequent characters may be letters, digits, underscores, or dollar signs -->

<!-- 
    Hyphens:
    first-name, last-name, master-card, inter-city (not allowed in js reserve for subtract)

    Underscore:
    first_name, last_name, master_card, inter_city

    Upper Camel Case (Pascal Case):
    FirstName, LastName, MasterCard, InterCity.

    Lower Camel Case:
    JavaScript programmers tend to use camel case that starts with a lowercase letter:
    firstName, lastName, masterCard, interCity

    Charset:
    JavaScript uses the Unicode character set
 -->