<!-- toPrecision() returns a string, with a number written with a specified length -->
<html>
<body>

<h2>toPrecision() method</h2>
<p id="demo"></p>

<script>
var x = 9.656;
document.getElementById("demo").innerHTML = 
  x.toPrecision() + "<br>" +
  x.toPrecision(2) + "<br>" +
  x.toPrecision(4) + "<br>" +
  x.toPrecision(6);  
</script>

</body>
</html>