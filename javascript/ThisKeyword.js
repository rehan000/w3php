// Create an object:
var person = {
    firstName: "rehan",
    lastName: "fazal",
    id: 5567,
    fullName: function() {
        return this.firstName + " " + this.lastName;
    }
};

// Display data from the object:
document.getElementById("demo").innerHTML = person.fullName();