<!--     1. NOT NULL - Ensures that a column cannot have a NULL value -->
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mysql_db";

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $sql = "CREATE TABLE cars (
    id  INT NOT NULL,
    company VARCHAR(255) NOT NULL,
    model VARCHAR(255) NOT NULL,
    speed INT)";
  $conn->exec($sql);
    echo "table and coloumn created ";
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;
?>