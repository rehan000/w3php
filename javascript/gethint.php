<?php
// Array with names
$a[] = "vikash";
$a[] = "bittu";
$a[] = "rahul";
$a[] = "Aman";
$a[] = "pushker";
$a[] = "rehan";
$a[] = "mirtunjay";
$a[] = "kunal";
$a[] = "vishal";
$a[] = "prateek";
$a[] = "rohan";
$a[] = "saquib";
$a[] = "Nina";
$a[] = "anupam";
$a[] = "Abhisheak";
$a[] = "Amanda";
$a[] = "rambo";
$a[] = "Cindy";
$a[] = "jhon";
$a[] = "Eve";
$a[] = "maddy";
$a[] = "Sunniva";
$a[] = "pikku";
$a[] = "sunny";
$a[] = "mia";
$a[] = "Liza";
$a[] = "Elizabeth";
$a[] = "Ellen";
$a[] = "Wenche";
$a[] = "Vicky";

// get the q parameter from URL
$q = $_REQUEST["q"];

$hint = "";

// lookup all hints from array if $q is different from ""
if ($q !== "") {
  $q = strtolower($q);
  $len=strlen($q);
  foreach($a as $name) {
    if (stristr($q, substr($name, 0, $len))) {
      if ($hint === "") {
        $hint = $name;
      } else {
        $hint .= ", $name";
      }
    }
  }
}

// Output "no suggestion" if no hint was found or output correct values
echo $hint === "" ? "no suggestion" : $hint;
?>