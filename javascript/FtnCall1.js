const person = {
    fullName: function() {
        return this.firstName + " " + this.lastName;
    }
}
const person1 = {
    firstName: "rehan",
    lastName: "fazal"
}
const person2 = {
    firstName: "aman",
    lastName: "fazal"
}
document.getElementById("demo").innerHTML = person.fullName.call(person1);