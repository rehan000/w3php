<!-- 
    Code	Result
    \b	    Backspace
    \f	    Form Feed
    \n	    New Line
    \r	    Carriage Return
    \t	    Horizontal Tabulator
    \v	    Vertical Tabulator
 -->
<html>
<body>

<h2>js Strings</h2>

<p id="demo"></p>
<p id="demo1"></p>
<p id="demo2"></p>

<script>
var x = "We are the so-called \"Vikings\" from the north.";
var y = 'It\'s alright';
var z = "The character \\ is called backslash.";
document.getElementById("demo").innerHTML = x; // escape sequence \" inserts a double quote in a string
document.getElementById("demo1").innerHTML = y; //sequence \'  inserts a single quote in a string
document.getElementById("demo2").innerHTML = z; //sequence \\  inserts a backslash in a string
</script>

</body>
</html>