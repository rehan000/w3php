<!-- Integers (numbers without a period or exponent notation) are accurate up to 15 digits -->
<html>
<body>

<p id="demo"></p>

<script>
var x = 999999999999999;
var y = 9999999999999999;
document.getElementById("demo").innerHTML = x + "<br>" + y;
</script>

</body>
</html>