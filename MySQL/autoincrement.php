<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mysql_db";

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $sql = "CREATE TABLE schoolboy (
    id int NOT NULL AUTO_INCREMENT,
    firstname varchar(255) NOT NULL,
    lastname varchar(255),
    age int,
    PRIMARY KEY (id)
)";
  $conn->exec($sql);
    echo "autoincrement constraints added";
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;
?>