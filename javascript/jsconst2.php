<!-- Declaring a constant array does NOT make the elements unchangeble
    const array can change
-->
<html>
<body>

<h2>js const</h2>
<p id="demo"></p>

<script>
const fruits = ["apple", "mango", "banana"];
fruits[0] = "pineapple";
fruits.push("strawberry");
document.getElementById("demo").innerHTML = fruits; 
</script>

</body>
</html>