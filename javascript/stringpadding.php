<!-- ECMAScript 2017 added two String methods: padStart and padEnd to 
support padding at the beginning and at the end of a string -->
<html>
<body>

<h2>padStart() method</h2>
<p id="demo"></p>

<script>
let str = "5";
str = str.padStart(4,0);
document.getElementById("demo").innerHTML = str;
</script>

</body>
</html>