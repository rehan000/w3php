<!-- Multi-line comments start with /* and end with */
    `text b/w /* and */ are ignored
-->
<html>
<body>

<h1 id="cmt"></h1>
<p id="cmt1"></p>

<script>
/*
The code below will change
the heading with id = "cmt"
and the paragraph with id = "cmt1"
*/
document.getElementById("cmt").innerHTML = "JavaScript Comments";
document.getElementById("cmt1").innerHTML = "My first paragraph.";
</script>

</body>
</html>