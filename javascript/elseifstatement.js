function myFunction() {
    var greeting;
    var time = new Date().getHours();
    if (time < 12) {
        greeting = "good morning";
    } else if (time < 18) {
        greeting = "good day";
    } else {
        greeting = "good evening";
    }
    document.getElementById("demo").innerHTML = greeting;
}