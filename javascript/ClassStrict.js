class Car {
    constructor(name, year) {
        this.name = name;
        this.year = year;

    }
    age() {
        // date = new Date();  // This will not work
        let date = new Date(); // This will work
        return date.getFullYear() - this.year;
    }
}

myCar = new Car("Ford", 2014);
document.getElementById("demo").innerHTML =
    "My car is " + myCar.age() + " years old.";