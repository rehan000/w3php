<?php
    // A destructor is called when the object is destructed or the script is stopped or exited
    class car {
        public $name;
        public $color;

        function __construct($name ,$color) {
            $this->name = $name; 
            $this->color = $color; 
        }
        function __destruct() {
            echo "Car is {$this->name}"; 
            echo " and color is {$this->color}";
        }
    }

    $Benz = new car("BenZ","Red");
?>