<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mysql_db";

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $sql = "CREATE TABLE Persons(id INT(100) UNSIGNED AUTO_INCREMENT PRIMARY KEY,lastname VARCHAR(255),
  firstname VARCHAR(255),city VARCHAR(255),email VARCHAR(255))";
  $conn->exec($sql);
    echo "table created";
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;
?>