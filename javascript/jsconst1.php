<!-- Declaring a constant object does NOT make the objects properties unchangeable
    1. constant obj can change
    2. reassign constant gives error
-->
<html>
<body>

<h2>js const</h2>
<p id="demo"></p>

<script>
const bike = {type:"harly", model:"500", color:"black"};
bike.color = "black";
bike.owner = "Johnson";
document.getElementById("demo").innerHTML = "bike owner is " + bike.owner; 
</script>

</body>
</html>
