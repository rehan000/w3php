<!-- naming and defining unique constraint to multiple coloumn -->
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mysql_db";

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $sql = "CREATE TABLE fastfoodcorner (
    id int NOT NULL,
    dishname varchar(255) NOT NULL,
    menu varchar(255),
    price int,
    CONSTRAINT ff_corner UNIQUE (id,dishname)
);";
  $conn->exec($sql);
    echo "table created with unique constraints";
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;
?>