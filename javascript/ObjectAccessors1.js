// setters allow you to set properties via methods
var person = {
    firstName: "rohan",
    lastName: "singh",
    language: "NO",
    set lang(value) {
        this.language = value;
    }
};
// Set a property using set:
person.lang = "en";
document.getElementById("demo").innerHTML = person.language;