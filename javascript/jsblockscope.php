<!-- Declaring a variable with const is similar to let when it comes to Block Scope -->
<html>
<body>

<h2>declaring value using const</h2>

<p id="demo"></p>

<script>
var  x = 10;

{  
  const x = 2;
} 
document.getElementById("demo").innerHTML = x;
</script>

</body>
</html>