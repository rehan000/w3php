<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "myPDOdb";

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $conn->beginTransaction();

  $conn->exec("INSERT INTO Students (firstname, lastname, email) VALUES ('ravi', 'singh', 'ravi@example.com')");
  $conn->exec("INSERT INTO Students (firstname, lastname, email) VALUES ('sonu', 'singh', 'sonu@example.com')");
  $conn->exec("INSERT INTO Students (firstname, lastname, email) VALUES ('salim', 'khan', 'salim@example.com')");

  $conn->commit();
  echo "New records created successfully";
} catch(PDOException $e) {
  $conn->rollback();
  echo "Error: " . $e->getMessage();
}

$conn = null;
?>