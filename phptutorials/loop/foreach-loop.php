<!-- Syntax
foreach ($array as $value) {
  code to be executed;
} -->
<?php
// foreach loop works only on arrays, and is used to loop through each key/value pair in an array.
    $car = array("BMW", "FERARI", "WV", "BENZ");
    foreach ($car as $value) {
      echo "$value <br>";
    }

    $age = array("rohit"=>"21", "varun"=>"31", "aman"=>"32");
    foreach($age as $x => $val) {
    echo "$x = $val<br>";
    }
?>