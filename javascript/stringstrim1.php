<!-- IE 8 does not support String.trim()  instead use replace-->
<html>
<body>

<h2>String.trim()</h2>
<script>
var str = "     Hello World!     ";
alert(str.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, ''));
</script>

</body>
</html>