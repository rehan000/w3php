<?php
    trait message1 {
        public function msg1() {
            echo "AwSm Day ";
        }
    }

    trait message2 {
        public function msg2() {
            echo "it's raining.";
        }
    }

    class Exp {
        use message1;
    }

    class Exp1 {
        use message1, message2;
    }

    $obj = new Exp();
    $obj->msg1();
    echo "<br>";

    $obj2 = new Exp1();
    $obj2->msg1();
    $obj2->msg2();
?>