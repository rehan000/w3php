<html>
<body>

<h2>js statement</h2>
<p id="stmt"></p>

<script>
var x, y, z;
x = 5;
y = 6;        
z = x + y;

document.getElementById("stmt").innerHTML =
"The value of z is " + z + ".";  
</script>

</body>
</html>