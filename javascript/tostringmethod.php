<html>
<body>

<h2>toString() method</h2>
<p id="demo"></p>

<script>
var x = 123;
document.getElementById("demo").innerHTML =
  x.toString() + "<br>" +
   (123).toString() + "<br>" +
   (100 + 23).toString();
</script>

</body>
</html>