<!-- you can also empty an object by setting it to undefined
    undefined and null are equal in value but different in type
 -->
<html>
<body>

<h2>js</h2>
<p id="demo"></p>

<script>
var person = {firstName:"rehan", lastName:"fazal", age:24, eyeColor:"brown"};
person = undefined;
document.getElementById("demo").innerHTML = person;
</script>

</body>
</html> 