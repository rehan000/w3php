<!-- When a class derives from another class.
The child class will inherit all the public and protected properties
and methods from the parent class. In addition, it can have its own
properties and methods -->
<?php
class car {
  public $name;
  public $color;
  public function __construct($name, $color) {
    $this->name = $name;
    $this->color = $color;
  }
  public function intro() {
    echo "Car is {$this->name} and the color is {$this->color}.";
  }
}
class bmw extends car {
  public function message() {
    echo "is it car or bike? ";
  }
}
$bmw = new bmw("bmws6", "red");
$bmw->message();
$bmw->intro();
?>