<!-- The MAX() function returns the largest value of the selected column -->
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mysql_db";
try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $stmt = $conn->prepare("SELECT MAX(postalcode) AS Largestpostalcode FROM students");
  $stmt->execute();
  $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
  print_r($result);
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;
?>