<!-- parseFloat() parses a string and returns a number. 
Spaces are allowed. Only the first number is returned -->
<html>
<body>

<h2>parseFloat() method</h2>

<p id="demo"></p>

<script>
document.getElementById("demo").innerHTML = 
  parseFloat("10") + "<br>" +
  parseFloat("10.33") + "<br>" +
  parseFloat("10 6") + "<br>" +  
  parseFloat("10 years") + "<br>" +
  parseFloat("years 10");    
</script>

</body>
</html>