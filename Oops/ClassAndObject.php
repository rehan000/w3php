<?php
  class car{
      public $name;
      public $color;

    function set_name($name){
        $this->name = $name;
    }
    function get_name(){
        return $this->name;
    }
    function set_color($color){
        $this->color = $color;
        }
        function get_color(){
            return $this->color;
        }
    }
  $bmw = new car();
  $benz = new car();
  $bmw->set_name('bmw');
  $bmw->set_color('red');
  $benz->set_name('benz');
  $benz->set_color('blue');

  echo $bmw->get_name();
  echo "<br>";
  echo $bmw->get_color();
  echo "<br>";
  echo $benz->get_name();
  echo "<br>";
  echo $benz->get_color();
  echo "<br>";
?>