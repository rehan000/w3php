<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mysql_db";

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $sql = "ALTER TABLE Orders
  DROP FOREIGN KEY FK_PersonOrder";
  $conn->exec($sql);
    echo "table created with foreign-key constraints";
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;
?>