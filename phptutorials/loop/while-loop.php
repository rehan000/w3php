<?php
// Syntax
// while (condition is true) {
//   code to be executed;
// }
$x = 1;

while($x <= 3) {
  // $x = 1; - Initialize the loop counter ($x), and set the start value to 1
  // $x <= 5 - Continue the loop as long as $x is less than or equal to 3
  // $x++; - Increase the loop counter value by 1 for each iteration
  echo "number: $x <br>";
  $x++;
}
echo "<h2>Another example</h2><br>";

$x = 0;

while($x <= 200) {
  // $x = 0; Initialize the loop counter ($x), and set the start value to 0
  // $x <= 200 - Continue the loop as long as $x is less than or equal to 200
  // $x+=10; - Increase the loop counter value by 10 for each iteration
  echo "number: $x <br>";
  $x+=10;
}
?>