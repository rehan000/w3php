<?php
    // json_encode()

    $age = array("max"=>23, "ravi"=>22, "peter"=>79);
    echo json_encode($age);
    echo "<br>";

    $cars = array("WV", "BMW", "BENZ");
    echo json_encode($cars);
    echo "<br>";

    // json_decode()

    $jsonobj = '{"max":23,"ravi":22,"peter":79}';
    var_dump(json_decode($jsonobj));
    echo "<br>";

    $jsonobj = '{"max":23,"ravi":22,"peter":79}';
    var_dump(json_decode($jsonobj, true));
    echo "<br>";

    // PHP - Accessing the Decoded Values
    $jsonobj = '{"max":23,"ravi":22,"peter":79}';

    $obj = json_decode($jsonobj);
    echo $obj->max;
    echo "<br>";
    echo $obj->ravi;
    echo "<br>";
    echo $obj->peter;
    echo "<br>";

    $jsonobjs = '{"max":23,"ravi":22,"peter":79}';

    $arr = json_decode($jsonobjs, true);
    echo $arr["max"];
    echo "<br>";
    echo $arr["ravi"];
    echo "<br>";
    echo $arr["peter"];
    echo "<br>";

    // Looping Through the Values foreach()
    $jsonobj = '{"max":23,"ravi":22,"peter":79}';

    $obj = json_decode($jsonobj);

    foreach($obj as $key => $value) {
    echo $key . " => " . $value . "<br>";
    }

    $jsonobj = '{"max":23,"ravi":22,"peter":79}';

    $arr = json_decode($jsonobj, true);

    foreach($arr as $key => $value) {
    echo $key . " => " . $value . "<br>";
    }
?>