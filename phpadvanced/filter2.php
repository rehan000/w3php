<html>

<body>
    <?php
        //filter_var() ftn remove all the html tag
        $str = "<h1>Hello World!</h1>";
        $newstr = filter_var($str, FILTER_SANITIZE_STRING);
        echo "$newstr <br><br>";

        //Validate an Integer
        $int = 100;
        if (!filter_var($int, FILTER_VALIDATE_INT) === false) {
            echo "integer is valid <br><br>";
        }else {
            echo "integer is not valid <br><br>";
        }
        
        //filter_var() and Problem With 0
        $int = 0;
        if (filter_var($int, FILTER_VALIDATE_INT) === 0 || !filter_var($int, FILTER_VALIDATE_INT) === false)
         {
            echo("Integer is valid <br><br>");
          } else {
            echo("Integer is not valid <br><br>");
          }

          //Validate an IP Address
          $ip = "127.0.0.1";
          if (!filter_var($ip,FILTER_VALIDATE_IP) === false) {
              echo  "valid ip address = " . $ip . "<br><br>";
          }else {
              echo "it is not a valid ip address <br><br>";
          }

          //Sanitize and Validate an Email Address

            $email = "rehan123@example.com";
            $email = filter_var($email, FILTER_SANITIZE_EMAIL);

            if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            echo("$email is a valid email address <br><br>");
            } else {
            echo("$email is not a valid email address <br><br>");
            }

        // Sanitize and Validate a URL

            $url = "https://www.w3schools.com";
            $url = filter_var($url, FILTER_SANITIZE_URL);

            if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
            echo("$url is a valid URL");
            } else {
            echo("$url is not a valid URL");
            }
    ?>
</body>

</html>