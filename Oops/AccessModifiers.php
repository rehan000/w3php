<!-- 
public - the property or method can be accessed from everywhere. This is default
protected - the property or method can be accessed within the class and by classes derived from that class
private - the property or method can ONLY be accessed within the class
-->
<?php
    class player {
        public $name;
        public $goals;
        public $weight;

        function set_name($n) {
            $this->name = $n;
        }
            function get_name() {
                return $this->name;
            }
        protected function set_goals($n) {
            $this->goals = $n;
        }
        private function set_weight($n) {
            $this->weight = $n;
        }
    }

    $p = new player();
    $p->set_name("messi"); //ok
    echo $p->get_name();
    $p->set_goals("5"); //ERROR
    $p->ser_weight("73"); //ERROR
?>