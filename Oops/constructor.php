<?php
    class car {
        public $name;
        public $color;

        function __construct($name,$color){
            $this->name = $name;
            $this->color = $color;
        }
        function get_name() {
            return $this->name;
        }
        function get_color() {
            return $this->color;
        }
    }
    $bmw = new car("BMW","blue");
    echo $bmw->get_name();
    echo "<br>";
    echo $bmw->get_color(); 
?>