var person = {
    firstName: "John",
    lastName: "Rambo",
    language: "en",
    get lang() {
        return this.language.toUpperCase();
    }
};
// Display data from the object using a getter:
document.getElementById("demo").innerHTML = person.lang;