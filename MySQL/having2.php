<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "join";
try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $stmt = $conn->prepare("SELECT employees.lastname, COUNT(orders.orderid) AS NumberOforders
  FROM orders
  INNER JOIN employees ON orders.employeid = employees.employeesid
  WHERE lastname = 'Davolio' OR lastname = 'Fuller'
  GROUP BY lastname
  HAVING COUNT(orders.orderid) > 25");
  $stmt->execute();
  $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
  print_r($result);
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;
?>