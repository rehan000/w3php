<!-- js functionn is executes if someone calls it
  function is defined with the function keyword, followed by a name, followed by parentheses ()
 -->
<html>
<body>

<h2>js  function</h2>

<p id="demo"></p>

<script>
function myFunction(p1, p2) {
  return p1 * p2;
}
document.getElementById("demo").innerHTML = myFunction(4, 3);
</script>

</body>
</html>