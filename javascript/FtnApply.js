const person = {
    fullName: function() {
        return this.firstName + " " + this.lastName;
    }
}

const person1 = {
    firstName: "ravi",
    lastName: "singh"
}

document.getElementById("demo").innerHTML = person.fullName.apply(person1);