<!-- in-built property is used to find the lenght -->
<html>
<body>

<h2>js String properties</h2>

<p id="demo"></p>

<script>
var txt = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
var sln = txt.length;
document.getElementById("demo").innerHTML = sln;
</script>

</body>
</html>