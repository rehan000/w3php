<!-- use to drop the table
    DROP TABLE Shippers;
 -->
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mysql_db";

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $sql = "DROP TABLE students";
  $conn->exec($sql);
    echo "table deleted";
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;
?>