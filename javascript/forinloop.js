// for/in statement loops through the properties of an object
var txt = "";
var person = { fname: "rehan", lname: "fazal", age: 24 };
var x;
for (x in person) {
    txt += person[x] + " ";
}
document.getElementById("demo").innerHTML = txt;