var person = { firstName: "rehan", lastName: "fazal" };

// Define a getter
Object.defineProperty(person, "fullName", {
    get: function() { return this.firstName + " " + this.lastName; }
});

// Display full name
document.getElementById("demo").innerHTML = person.fullName;