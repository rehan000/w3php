<?php
abstract class ParentClass {
  // Abstract method with an argument
  abstract protected function prefixName($name);
}

class ChildClass extends ParentClass {
  // The child class may define optional arguments that are not in the parent's abstract method
  public function prefixName($name, $separator = ".", $greet = "dear") {
    if ($name == "rehan fazal") {
      $prefix = "Mr";
    } elseif ($name == "rihana") {
      $prefix = "Mrs";
    } else {
      $prefix = "";
    }
    return "{$greet} {$prefix}{$separator} {$name}";
  }
}

    $class = new ChildClass;
    echo $class->prefixName("rehan fazal");
    echo "<br>";
    echo $class->prefixName("rihana");
?>