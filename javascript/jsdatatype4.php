<!-- js types are dynamic -->
<html>
<body>

<h2>js datatypes</h2>
<p id="demo"></p>

<script>
var y;         // y is undefined
y = 12;         // y is a Number
y = "rehan";      // y is a String

document.getElementById("demo").innerHTML = y;
</script>

</body>
</html>