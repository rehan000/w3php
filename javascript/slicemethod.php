<!-- start position, and the end position (end not included).

This example slices out a portion of a string from position 7 to position 12 (13-1) -->
<html>
<body>

<h2>slice() method</h2> 
<p id="demo"></p>

<script>
var str = "Apple, Banana, Kiwi";
var res = str.slice(7,13);
document.getElementById("demo").innerHTML = res; 
</script>

</body>
</html>