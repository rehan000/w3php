<!-- substring() will slice out the rest of the string -->
<html>
<body>

<h2>substring() method</h2>
<p id="demo"></p>

<script>
var str = "Apple, Banana, Kiwi";
var res = str.substring(7,13);
document.getElementById("demo").innerHTML = res;
</script>

</body>
</html>