<!-- use the replace solution above to add a trim function to the JavaScript String.prototype -->
<html>
<body>

<script>
if (!String.prototype.trim) {
  String.prototype.trim = function () {
    return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
  };
}
var str = "     Hello World!    ";
alert(str.trim());
</script>

</body>
</html>