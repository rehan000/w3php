<!-- Syntax
ALTER TABLE table_name
ADD column_name datatype; -->

<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "join";

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $sql = "ALTER TABLE customers
  ADD email varchar(255)";
  $conn->exec($sql);
    echo "coloum added successfully";
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;
?>