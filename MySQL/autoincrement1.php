<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mysql_db";

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $sql = "ALTER TABLE schoolboy AUTO_INCREMENT=100";
  $conn->exec($sql);
    echo "autoincrement constraints added";
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;
?>