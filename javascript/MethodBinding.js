// Create an object:
var person = {
    firstName: "ravi",
    lastName: "singh",
    id: 3345,
    myFunction: function() {
        return this;
    }
};

// Display data from the object:
document.getElementById("demo").innerHTML = person.myFunction();