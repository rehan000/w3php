<?php
    $x = 15; //global scope

    // global scope-> variable declare outside of function

    function myTest() {
    // using x inside a function genrate err.
        echo "<p>Variable x inside function is: $x</p>";
    }
    myTest();
    echo "<p>Variable x outside function is: $x</p>";
    
?>