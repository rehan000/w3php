<!-- shift() method removes the first array element and
 "shifts" all other elements to a lower index -->
<html>
<body>

<h2>shift()</h2>

<p id="demo1"></p>
<p id="demo2"></p>

<script>
var fruits = ["Banana", "Orange", "Apple", "Mango"];
document.getElementById("demo1").innerHTML = fruits;
fruits.shift();
document.getElementById("demo2").innerHTML = fruits;
</script>

</body>
</html>