<!-- It was used to "jump out" of a switch statement.
The break statement can also be used to jump out of a loop. -->

<?php
    for ($x = 0; $x < 10; $x++) {
    if ($x == 4) {
        break;
    }
    echo "The number is: $x <br>";
    }

        echo "<h2>Another Example</h2>";
    // continue statement breaks one iteration (in the loop),
    // if a specified condition occurs, and continues with the next iteration in the loop.

    for ($y = 0; $y < 10; $y++) {
    if ($y == 4) {
        continue;
    }
    echo "The number is: $y <br>";
    }

    // we can use Break and Continue in While Loop.
?>