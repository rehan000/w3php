<?php

    $cookie_name = "user";
    $cookie_value ="rehan fazal";
    
    setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400s = 1day
?>
<html>

<head>
    <title>Cookie</title>
</head>

<body>
    <?php
    if(!isset($_COOKIE[$cookie_name])){
        echo "cookie name " . $cookie_name . " is not set!";    
    }else{
        echo "cookie " . $cookie_name . " is set <br>";
        echo "value is : " .$_COOKIE[$cookie_name];
    }
?>
</body>

</html>