<!-- strings can be objects -->
<html>
<body>
<p id="demo"></p>

<script>
var x = "rehan";        // x is a string
var y = new String("rehan");  // y is an object

document.getElementById("demo").innerHTML =
typeof x + "<br>" + typeof y;
</script>

</body>
</html>