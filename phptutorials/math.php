<?php
    // pi() function
    echo pi();
    echo "<br>";

    // min() and max() function
    echo min(0, 32, 97, -33, -70);
    echo "<br>";
    echo max(0, 32, 97, -33 -70);
    echo "<br>";

    // abs() returns absolute +ve value of a number
    echo abs(-7.12);
    echo "<br>";

    // sqrt() returns the square root of a number
    echo sqrt(64);
    echo "<br>";

    // round() rounds a floting point number to its nearest integer
    echo round(7.653);
    echo "<br>";
    echo round(7.153);
    echo "<br>";

    // rand() genrates a random number and a you could optinal min and max paramiters rand(min,max)
    echo rand();
    echo "<br>";
    echo rand(0,10);
    echo "<br>";
?>