<?php
    // Callback Functions array_map()

    function callback($item) {
        return strlen($item);
    }
    $strings = ["red","blue","orange","yellow"];
    $lengths = array_map("callback", $strings);
    print_r($lengths);
    echo "<br>";

    // Anonymous function

    $strings = ["apple", "orange", "banana", "coconut"];
    $lengths = array_map( function($item) { return strlen($item); } , $strings);
    print_r($lengths);
    echo "<br>";

    // Callbacks in User Defined Functions

    function exclaim($str) {
        return $str . "! ";
      }
      
      function ask($str) {
        return $str . "? ";
      }
      
      function printFormatted($str, $format) {
        echo $format($str);
      }
      printFormatted("Hello world", "exclaim");
      printFormatted("Hello world", "ask");
?>