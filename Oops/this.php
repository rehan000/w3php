<?php
    
    //Inside the class (by adding a set_name() method and use $this)
    class Fruit {
        public $name;
        function set_name($name) {
        $this->name = $name;
        }
    }
    $apple = new Fruit();
    $apple->set_name("Apple");

    // Outside the class (by directly changing the property value)
    class color {
        public $name;
      }
      $apple = new color();
      $apple->name = "black";

    /**You can use the instanceof keyword to check if an object belongs to a 
    specific class */ 
    $apple = new Fruit();
    var_dump($apple instanceof Fruit);
?>