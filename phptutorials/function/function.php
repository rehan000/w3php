<!-- php has more then 1000 php built-in-functions -->
<?php
    // Syntax
    // function functionName() {
    //     code to be executed;
    //   }
    function writeMsg() {
        // example of user-defined function
    echo "hello world";
    }

    writeMsg(); //call the function

    echo "<h2>Another Example</h2>";

    function colorName($cname) {
        echo "$cname ref.<br>";
      }
      
      colorName("red");
      colorName("orange");
      colorName("blue");
      colorName("pin");
      colorName("yellow");

      echo "<h2>Another Example</h2>";

      function branchName($batch,$rollNo){
          echo "Batch:-  $batch RollNo:-  $rollNo<br>";
      }
      branchName("CSE","534");
      branchName("MECH","334");
      branchName("CIVIL","102");
      branchName("ECE","203");

    //   To specify strict we need to set declare(strict_types=1)
    // This must be on the very first line of the PHP file.

    // declare(strict_types=1); // strict requirement

    // function addNumbers(int $a, int $b) {
    // return $a + $b;
    // }
    // echo addNumbers(5, "5 days");// since strict is enabled and "5 days" is not an integer, 
    // an error will be thrown
?>