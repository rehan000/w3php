function myFunction() {
    var hour = new Date().getHours();
    var greeting;
    if (hour < 12) {
        greeting = "good-morning";
    } else {
        greeting = "good evening";
    }
    document.getElementById("demo").innerHTML = greeting;
}