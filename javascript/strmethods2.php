<!-- Both indexOf(), and lastIndexOf() return -1 if the text is not found -->
<html>
<body>

<h2>js String Methods</h2>

<p id="demo"></p>

<script>
var str = "Please locate where 'locate' occurs!";
var pos = str.indexOf("rehan");
document.getElementById("demo").innerHTML = pos;
</script>

</body>
</html>
