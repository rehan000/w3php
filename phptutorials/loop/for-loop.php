<?php
    // Syntax
    // for (init counter; test counter; increment counter) {
    //   code to be executed for each iteration;
    // }

    // Parameters:
    //  $x = 0 -> init counter: Initialize the loop counter value
    //  $x <= 5 -> test counter: Evaluated for each loop iteration. If it evaluates to TRUE, the loop continues. If it evaluates to FALSE, the loop ends.
    //  $x++ -> increment counter: Increases the loop counter value

    for ($x = 0; $x <= 5; $x++) {
        // print numbers from 0 to 5
    echo "No: $x <br>";
    }

    echo "<h2>Another example</h2> <br>";

    for ($x = 0; $x <= 100; $x+=10) {
        // increase loop counter by 10 each time
        echo "The number is: $x <br>";
      }
?>