<!-- ES2015 introduced two important new JavaScript keywords: let and const -->
<html>
<body>

<h2>js scope</h2>

<p>GLOBAL variable can be accessed from any script or function.</p>

<p id="demo"></p>

<script>
var carName = "Volvo";
myFunction();

function myFunction() {
  document.getElementById("demo").innerHTML = "I can display " + carName;
}
</script>

</body>
</html>