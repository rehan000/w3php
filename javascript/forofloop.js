// for/of statement loops through the values of an iterable object
let cars = ["BMW", "Benz", "ferarri"];
let text = "";

for (let x of cars) {
    text += x + "<br>";
}

document.getElementById("demo").innerHTML = text;