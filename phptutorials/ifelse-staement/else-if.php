<!-- if...elseif...else statement executes different codes for more than two conditions -->
<?php
    // Syntax
    // if (condition) {
    //   code to be executed if this condition is true;
    // } elseif (condition) {
    //   code to be executed if first condition is false and this condition is true;
    // } else {
    //   code to be executed if all conditions are false;
    // }

    $t = date("H");

    if ($t < "10") {
    echo "hello word";
    } elseif ($t < "20") {
    echo "good day";
    } else {
    echo "good night";
    }
?>