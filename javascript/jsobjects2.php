<html>
<body>

<h2>js Objects</h2>

<p id="demo"></p>

<script>
// Create an object:
var bike = {type:"harly devidson", model:"street500", color:"black"};

// Display some data from the object:
document.getElementById("demo").innerHTML = "The bike type is " + bike.type;
</script>

</body>
</html>