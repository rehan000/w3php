<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mysql_db";
// CONCAT_WS helps to comine columns
try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $stmt = $conn->prepare("SELECT fullname, CONCAT_WS(', ', Address, PostalCode, City, Country) AS Address
  FROM students");
  $stmt->execute();
  $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
  print_r($result);
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;
?>