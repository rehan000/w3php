<?php
// Syntax
// do {
//   code to be executed;
// } while (condition is true);
$x = 1;

do {
  echo "The number is: $x <br>";
  $x++;
} while ($x <= 5);

echo "<h2>Another example</h2> <br>";

$y = 6;

do {
  echo "The number is: $x <br>";
  $y++;
} while ($y <= 5);
?>