<!-- accessing Object Properties
    way2[]
 -->
<html>
<body>

<h2>js Objects</h2>

<p id="demo"></p>

<script>
// Create an object:
var person = {
  firstName: "rehan",
  lastName : "fazal",
  id     :  5566
};
// Display some data from the object:
document.getElementById("demo").innerHTML =
person["firstName"] + " " + person["lastName"];
</script>

</body>
</html>