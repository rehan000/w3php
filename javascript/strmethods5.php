<!-- The search() method cannot take a second start position argument.
The indexOf() method cannot take powerful search values (regular expressions) -->
<html>
<body>

<h2>search() method</h2>
<p id="demo"></p>

<script>
var str = "Please locate where 'locate' occurs!";
var pos = str.search("locate");
document.getElementById("demo").innerHTML = pos;
</script>

</body>
</html>