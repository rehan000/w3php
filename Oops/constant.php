<?php
class Says {
  const LEAVING_MESSAGE = "hello world";
}

echo says::LEAVING_MESSAGE;

/** we can access a constant from inside the class by 
 * using the self keyword followed by the scope resolution operator (::)
 *  followed by the constant name **/
echo "<br>";
class hi {
    const LEAVING_MESSAGE = "hello";
    public function bye() {
      echo self::LEAVING_MESSAGE;
    }
  }
  
  $hi = new hi();
  $hi->bye();
?>