<!-- maximum number of decimals is 17, but floating point arithmetic is not always 100% accurate -->
<html>
<body>

<p id="demo"></p>

<script>
var x = 0.2 + 0.1;
document.getElementById("demo").innerHTML = "0.2 + 0.1 = " + x;
</script>

</body>
</html>
