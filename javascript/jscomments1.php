<!-- Single line comments start with // -->
<html>
<body>

<h1 id="ltt"></h1>
<p id="mpp"></p>

<script>
// Change heading:
document.getElementById("ltt").innerHTML = "js comments";
// Change paragraph:
document.getElementById("mpp").innerHTML = "hello world";
</script>

</body>
</html>