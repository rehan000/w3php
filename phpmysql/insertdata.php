<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "myPDOdb";

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $sql = "INSERT INTO Students (firstname, lastname, email) VALUES ('rehan', 'fazal', 'rf26538@gmail.com')";
  $conn->exec($sql);
  echo "New record created successfully";
} catch(PDOException $e) {
  echo $sql . "<br>" . $e->getMessage();
}

$conn = null;
?>