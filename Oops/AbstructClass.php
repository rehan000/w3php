<!-- abstract class ParentClass {
  abstract public function someMethod1();
  abstract public function someMethod2($name, $color);
  abstract public function someMethod3() : string;
} -->
<?php
// Parent class
abstract class Car {
  public $name;
  public function __construct($name) {
    $this->name = $name;
  }
  abstract public function intro() : string;
}

// Child classes
class Audi extends Car {
  public function intro() : string {
    return "Car1 $this->name!";
  }
}

class Benz extends Car {
  public function intro() : string {
    return "Car2 $this->name!";
  }
}

class Bmw extends Car {
  public function intro() : string {
    return "Car3 $this->name!";
  }
}

// Create objects from the child classes
$audi = new audi("Audi");
echo $audi->intro();
echo "<br>";

$Benz = new Benz("Benz");
echo $Benz->intro();
echo "<br>";

$Bmw = new Bmw("Bmw");
echo $Bmw->intro();
?>
