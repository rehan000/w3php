<!-- Multidimensional Arrays
   1. For a two-dimensional array you need two indices to select an element
   2. For a three-dimensional array you need three indices to select an element
 -->
 <?php
$cars = array (
    // two dimentional array ("name",stock,sold)
  array("Volvo",22,18),
  array("BMW",15,13),
  array("Saab",5,2),
  array("Land Rover",17,15)
);
  
echo $cars[0][0].": In stock: ".$cars[0][1].", sold: ".$cars[0][2].".<br>";
echo $cars[1][0].": In stock: ".$cars[1][1].", sold: ".$cars[1][2].".<br>";
echo $cars[2][0].": In stock: ".$cars[2][1].", sold: ".$cars[2][2].".<br>";
echo $cars[3][0].": In stock: ".$cars[3][1].", sold: ".$cars[3][2].".<br>";

echo "<h2>We can also put a for loop inside another for loop
 to get the elements of the cars
 array (we still have to point to the two indices)</h2>";

 for ($r = 0; $r < 4; $r++) {
    echo "<p><b>Row number $r</b></p>";
    echo "<ul>";
    for ($c = 0; $c < 3; $c++) {
      echo "<li>".$cars[$r][$c]."</li>";
    }
    echo "</ul>";
  }
?>