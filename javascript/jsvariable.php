<!-- Using let and const (ES6)
    using the var keyword was the only way to declare a JavaScript variable
 -->
<html>
<body>

<h2>js variable</h2>

<p>In this example, x, y, and z are variables.</p>

<p id="var1"></p>

<script>
var x = 3;
var y = 4;
var z = x + y;
document.getElementById("var1").innerHTML =
"The value of z is: " + z;
</script>

</body>
</html>