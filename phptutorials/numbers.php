<?php
// integers
$x = 5785;
var_dump(is_int($x));
echo "<br>";

$x = 22.85;
var_dump(is_int($x));
echo "<br>";

// float
$x = 7.321;
var_dump(is_float($x));
echo "<br>";

// infinity
$x = 1.2e231;
var_dump($x);
echo "<br>";

// not a number NAN
 $x = acos(23);
 var_dump($x);
 echo "<br>";

//  numarical string specify wheter a number is numric or not
$x = 365;
var_dump(is_numeric($x));

$x = "685";
var_dump(is_numeric($x));

$x = "2.75" + 1000;
var_dump(is_numeric($x));

$x = "Hello";
var_dump(is_numeric($x));
echo "<br>";
// casting string and flots to integer
// (int),(integer), or intval() are often used to conver a value into integer

//c asting float to int
$x = 1422535.33;
$int_cast = (int)$x;
echo $int_cast;
echo "<br>";

// casting string to int
$x = "2344447.77";
$int_cst = intval($x);
echo $int_cst;
?>