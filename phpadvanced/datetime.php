<?php
echo "Today is : " . date("Y/m/d") . "<br>";
echo "Today is :" . date("Y.m.d") . "<br>";
echo "Today is : " . date("Y-m-d") . "<br>";
echo "Today is : " . date("l");
?>
<br><br>
<!-- copyright year -->
<!DOCTYPE html>
<html>
<body>

copyright year © 2010-<?php echo date("Y");?>
<br><br>
<?php echo "The time is " . date("h:i:sa");?>

<br><br>
diffrent time zone
<?php
date_default_timezone_set("America/New_York");
echo "The time is " . date("h:i:sa");
?>

<br><br>
<?php
// mktime(hour, minute, second, month, day, year)
$d=mktime(13, 51, 54, 04, 20, 2021);
echo "Created date is " . date("Y-m-d h:i:sa", $d);
?>

<br><br>
<?php
//strtotime(time, now)
$d=strtotime("13:55pm April 20 2021");
echo "Created date is " . date("Y-m-d h:i:sa", $d);

$d=strtotime("tomorrow");
echo date("Y-m-d h:i:sa", $d) . "<br>";

$d=strtotime("next Saturday");
echo date("Y-m-d h:i:sa", $d) . "<br>";

$d=strtotime("+2 Months");
echo date("Y-m-d h:i:sa", $d) . "<br>";

$startdate=strtotime("Saturday");
$enddate=strtotime("+6 weeks", $startdate);
while ($startdate < $enddate) {
  echo date("M d", $startdate) . "<br>";
  $startdate = strtotime("+1 week", $startdate);
}
?>
</body>
</html>