<!-- toString() method to output numbers from base 2 to base 36 -->
<html>
<body>

<p>toString() method</p>

<p id="demo"></p>

<script>
var myNumber = 32;
document.getElementById("demo").innerHTML =
"32 = " + "<br>" + 
" Decimal " + myNumber.toString(10) + "<br>" +
" Hexadecimal " + myNumber.toString(16) + "<br>" +
" Octal " + myNumber.toString(8) + "<br>" +
" Binary " + myNumber.toString(2);
</script>

</body>
</html>