<!-- lastIndexOf() method -->
<html>
<body>

<h2>JavaScript String Methods</h2>
<p id="demo"></p>

<script>
var str = "Please locate where 'locate' occurs";
var pos = str.lastIndexOf("locate");
document.getElementById("demo").innerHTML = pos;
</script>

</body>
</html>