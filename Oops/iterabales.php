<!-- An iterable is any value which can be looped through with a foreach() loop -->
<?php
function printIterable(iterable $myIterable) {
  foreach($myIterable as $item) {
    echo $item;
  }
}

$arr = ["x", "y", "z"];
printIterable($arr);
?>