function Other() {
    var message, x;
    message = document.getElementById("p01");
    message.innerHTML = "";
    x = document.getElementById("demo").value;
    try {
        if (x == "") throw "empty";
        if (isNaN(x)) throw "not a number";
        x = Number(x);
        if (x < 2) throw "too low";
        if (x > 20) throw "too high";
    } catch (err) {
        message.innerHTML = "Input is " + err;
    }
}

function Another() {
    var message, x;
    message = document.getElementById("p02");
    message.innerHTML = "";
    x = document.getElementById("demo1").value;
    try {
        if (x == "") throw "is empty";
        if (isNaN(x)) throw "is not a number";
        x = Number(x);
        if (x > 30) throw "is too high";
        if (x < 10) throw "is too low";
    } catch (err) {
        message.innerHTML = "Input " + err;
    } finally {
        document.getElementById("demo1").value = "";
    }
}