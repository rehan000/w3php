<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "join";
try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $stmt = $conn->prepare("SELECT COUNT(customersid), country
  FROM customers
  GROUP BY country
  HAVING COUNT(customersid) > 1");
  $stmt->execute();
  $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
  print_r($result);
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;
?>