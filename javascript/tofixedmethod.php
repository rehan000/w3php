<!-- toFixed() returns a string, with the number written with a specified number of decimals -->
<html>
<body>

<h2>toFixed() method</h2>

<p id="demo"></p>

<script>
var x = 9.656;
document.getElementById("demo").innerHTML =
  x.toFixed(0) + "<br>" +
  x.toFixed(2) + "<br>" +
  x.toFixed(4) + "<br>" +
  x.toFixed(6);
</script>

</body>
</html>