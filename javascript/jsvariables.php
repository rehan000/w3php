<!-- variabls are used to store data values
    js uses the var keyword to declare variables
    equal(=) sign is use to assign values
 -->
<html>
<body>

<h2>JavaScript Variables</h2>
<p id="var"></p>

<script>
var x;
x = 6;
document.getElementById("var").innerHTML = x;
</script>

</body>
</html>