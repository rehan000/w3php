<?php
echo "<h2>PHP is Fun!</h2>";
echo "Hello world!<br>";
echo "I'm about to learn PHP!<br>";
echo "This ", "string ", "was ", "made ", "with multiple parameters.<br>";

$txt = "Learn PHP";
$str = "learn.com";
$x = 1;
$y = 2;

echo "<h4>" . $txt . "</h4>";
echo "Study PHP at " . $str . "<br>";
echo $x + $y;
echo "<br>";

// print statement can be used with or without parentheses: print or print()

print "<h3>PHP is Fun!</h3>";
print "Hello world!<br>";

$txt2 = "php class";
$str2 = "know php";
$a = 15;
$b = 30;

print "$txt2 <br>"; // we can also use contatination.
print "i $str2 very well. <br>";
print $a + $b;

?>