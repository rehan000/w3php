<?php
    abstract class ParentClass {
        abstract protected function prefixName($name);
    }

    class ChildClass extends ParentClass {
        public function prefixName($name) {
            if ($name == "rehan fazal") {
              $prefix = "Mr.";
            } elseif ($name == "rihana") {
              $prefix = "Mrs.";
            } else {
              $prefix = "";
            }
            return "{$prefix} {$name}";
          }
        }
        
        $class = new ChildClass;
        echo $class->prefixName("rehan fazal");
        echo "<br>";
        echo $class->prefixName("rihana");
?>