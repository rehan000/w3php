<!-- sort() method sorts an array alphabetically -->
<html>
<body>

<h2>JavaScript Array Sort</h2>
<button onclick="myFunction()">click</button>

<p id="demo"></p>

<script>
var fruits = ["Banana", "Orange", "Apple", "Mango"];
document.getElementById("demo").innerHTML = fruits;

function myFunction() {
  fruits.sort();
  document.getElementById("demo").innerHTML = fruits;
}
</script>

</body>
</html>