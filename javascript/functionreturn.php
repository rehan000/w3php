<!-- js reaches a return statement, the function will stop executing -->
<html>
<body>

<h2>js function</h2>
<p id="demo"></p>

<script>
var x = myFunction(4, 3);
document.getElementById("demo").innerHTML = x;

function myFunction(a, b) {
  return a * b;
}
</script>

</body>
</html>