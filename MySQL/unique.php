<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mysql_db";

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $sql = "CREATE TABLE country (
    ID int NOT NULL,
    states varchar(255) NOT NULL,
    city varchar(255),
    pin int,
    UNIQUE (ID)
)";
  $conn->exec($sql);
    echo "table created with unique constraints";
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;
?>