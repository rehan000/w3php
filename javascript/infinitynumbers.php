<html>
<body>

<p>Infinity is returned if you calculate a number outside the largest possible number:</p>

<p id="demo"></p>

<script>
var mynumb = 4; 
var txt = "";
while (mynumb != Infinity) {
   mynumb = mynumb * mynumb;
   txt = txt + mynumb + "<br>";
}
document.getElementById("demo").innerHTML = txt;
</script>

</body>
</html>