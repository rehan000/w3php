<?php
    session_start();
?>
<html>

<head>
    <title>session distroy and unset</title>
</head>

<body>
    <?php
        //remove all session variables
        session_unset();

        //destroy the session
        session_destroy();

        echo "all session variable are removed and session is destroyed.";
    ?>
</body>

</html>