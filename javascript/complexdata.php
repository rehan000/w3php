<!-- typeof operator can return one of two complex types:
    function
    object
 -->
<html>
<body>

<h2>JavaScript typeof</h2>

<p id="demo"></p>

<script>
document.getElementById("demo").innerHTML = 
typeof {name:'rehan', age:24} + "<br>" +
typeof [1,2,3,4] + "<br>" +
typeof null + "<br>" +
typeof function myFunc(){};
</script>

</body>
</html>