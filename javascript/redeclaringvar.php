<html>
<body>
<!-- Redeclaring a variable using the var keyword can impose problems -->
<h2>declaring variable using var</h2>

<p id="demo"></p>

<script>
var  x = 10;
// Here x is 10
{  
  var x = 2;
  // Here x is 2
}
// Here x is 2
document.getElementById("demo").innerHTML = x;
</script>

</body>
</html>