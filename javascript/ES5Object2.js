var person = {
        firstName: "John",
        lastName: "Doe",
        language: "EN"
    }
    // Change Property
Object.defineProperty(person, "language", { enumerable: false });
// Display Properties
document.getElementById("demo").innerHTML = Object.keys(person);