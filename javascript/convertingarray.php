<!--  method toString() converts an array to a string of (comma separated) array values -->
<html>
<body>
<h2>toString()</h2>

<p id="demo"></p>

<script>
var fruits = ["Banana", "Orange", "Apple", "Mango"];
document.getElementById("demo").innerHTML = fruits.toString();
</script>

</body>
</html>