<html>
<body>

<h2>js statements</h2>

<p>JavaScript code blocks are written between { and }</p>

<button type="button" onclick="myFunction()">Reveal</button>

<p id="stmt2"></p>
<p id="stmt3"></p>

<script>
function myFunction() {
  document.getElementById("stmt2").innerHTML = "Hello";
  document.getElementById("stmt3").innerHTML = "How are you?";
}
</script>

</body>
</html>