<!-- concat() method can be used instead of the plus operator -->
<html>
<body>

<h2>concat() method</h2>
<p id="demo"></p>

<script>
var text1 = "Hello";
var text2 = "World!";
var text3 = text1.concat(" ",text2);
document.getElementById("demo").innerHTML = text3;
</script>

</body>
</html>