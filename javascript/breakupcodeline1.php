<!-- cannot break up a code line with a backslash \ -->
<html>
<body>

<h2>js Strings</h2>
<p id="demo"></p>

<script>
document.getElementById("demo").innerHTML = "Hello " +
"Dolly!";   //safest way to break a code line in a string is using string addition +
</script>

</body>
</html>