 <!-- use to delete data inside the table
     TRUNCATE TABLE table_name;
  -->
  <?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mysql_db";

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $sql = "TRUNCATE TABLE students";
  $conn->exec($sql);
    echo "table data deleted";
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;
?>