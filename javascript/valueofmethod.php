<!-- valueOf() returns a number as a number -->
<html>
<body>

<h2>valueOf() method</h2>

<p id="demo"></p>

<script>
var x = 123;

document.getElementById("demo").innerHTML = 
  x.valueOf() + "<br>" +
  (123).valueOf() + "<br>" +
  (100 + 23).valueOf();
</script>

</body>
</html>
