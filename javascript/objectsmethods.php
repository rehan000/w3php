<!-- by using ()-->
<html>
<body>
​
<h2>js Objects</h2>
​
<p id="demo"></p>
​
<script>
var person = {
  firstName: "rehan",
  lastName : "fazal",
  id     : 5566,
  fullName : function() {
    return this.firstName + " " + this.lastName;
  }
};
document.getElementById("demo").innerHTML = person.fullName();
</script>
​
</body>
</html>
​