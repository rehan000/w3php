<!-- array.sort(), is not accurate, it will favor some numbers over the others.
The most popular correct method, is called the Fisher Yates shuffle, 
and was introduced in data science as early as 1938! -->
<html>
<body>

<button onclick="myFunction()">click</button>

<p id="demo"></p>

<script>
var points = [40, 100, 1, 5, 25, 10];
document.getElementById("demo").innerHTML = points;  

function myFunction() {
var i, j, k;
  for (i = points.length -1; i > 0; i--) {
    j = Math.floor(Math.random() * i)
    k = points[i]
    points[i] = points[j]
    points[j] = k
  }
  document.getElementById("demo").innerHTML = points;
}
</script>

</body>
</html>