<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "join";
try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $stmt = $conn->prepare("SELECT city, country FROM customers
  WHERE country='germany'
  UNION ALL
  SELECT city, country FROM supplier
  WHERE country='germany'
  ORDER BY city");
  $stmt->execute();
  $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
  print_r($result);
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;
?>