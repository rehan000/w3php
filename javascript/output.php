<!--
    using document.getElementById(id) method.
    The innerHTML property defines the HTML content
 -->
<html>
<body>

<h1>hello world</h1>
<p>paragraph</p>

<p id="output"></p>

<script>
document.getElementById("output").innerHTML = 5 + 6*10;
</script>

</body>
</html>