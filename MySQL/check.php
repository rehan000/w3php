<!-- CHECK constraint is used to limit the value range that can be placed in a column -->
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mysql_db";

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $sql = "CREATE TABLE people (
    id int NOT NULL,
    lastname varchar(255) NOT NULL,
    firstname varchar(255),
    age int,
    CHECK (age>=18)
)";
  $conn->exec($sql);
    echo "table created with CHECK constraints";
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;
?>