<?php
class car {
  public $name;
  public $color;
  public function __construct($name, $color) {
    $this->name = $name;
    $this->color = $color;
  }
  protected function intro() {
    echo "Car is {$this->name} and the color is {$this->color}.";
  }
}
class bmw extends car {
  public function message() {
    echo "is it car or bike? ";
    $this->intro(); // Call protected method from within derived class - OK
  }
}
$bmw = new bmw("bmws6", "red");
$bmw->message();
?>