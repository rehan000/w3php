<!-- ES2015 introduced two important new JavaScript keywords: let and const -->
<!-- You cannot change a primitive value
     js const variables must be assigned a value when they are declared
 -->
<html>
<body>

<h2>js const</h2>
<p id="demo"></p>

<script>
try {
  const PI = 3.141592653589793;
  PI = 3.14;
}
catch (err) {
  document.getElementById("demo").innerHTML = err;
}
</script>

</body>
</html>