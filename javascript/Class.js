class Car {
    constructor(name, year) {
        this.name = name;
        this.year = year;
    }
}

myCar = new Car("Ferrari", 2020);
document.getElementById("cls").innerHTML =
    myCar.name + " " + myCar.year;