<?php
function getIterable():iterable {
  return ["x", "y", "z"];
}

$myIterable = getIterable();
foreach($myIterable as $item) {
  echo $item;
}
?>