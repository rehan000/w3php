class Car {
    constructor(name, year) {
        this.name = name;
        this.year = year;

    }
    age() {
        let date = new Date();
        return date.getFullYear() - this.year;
    }
}

let myCar = new Car("Benz", 2014);
document.getElementById("demo").innerHTML =
    "Car is " + myCar.age() + " years old.";