<!-- string is a sequence of characters, like "hello world" -->

<?php
// strlen() - returns the lenght of a string
    echo strlen("hello");
    echo "<br>";

// str_word_count() - count words in a string
    echo str_word_count("hello world");
    echo "<br>";

// strrev() - reverse a string
    echo strrev("hello world");
    echo "<br>";

// strpos() - search a text withing the string 
    echo strpos("hello world", "world");
    echo "<br>";

 // str_replace() - replace text within a string 
    echo str_replace("world", "bro", "hello world");
    echo "<br>";
?>