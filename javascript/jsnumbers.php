<!-- numbers can be written with or without decimals -->
<html>
<body>

<h2>js numbers</h2>
<p id="demo"></p>

<script>
var x1 = 12.00;
var x2 = 12;
var x3 = 3.14;
var y = 123e5;
var z = 123e-5;

document.getElementById("demo").innerHTML =
x1 + "<br>" + x2 + "<br>" + x3 + "<br>" + y + "<br>" + z;
</script>

</body>
</html>