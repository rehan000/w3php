function Person(first, last, age, eye) {
    this.firstName = first;
    this.lastName = last;
    this.age = age;
    this.eyeColor = eye;
}

var myFather = new Person("F", "H", 50, "blue");
var myMother = new Person("C", "K", 48, "green");

document.getElementById("demo").innerHTML =
    "My father is " + myFather.age + ". My mother is " + myMother.age;