 <!--CREATE TABLE table_name (
    column1 datatype constraint,
    column2 datatype constraint,
    column3 datatype constraint,
    ....
);
    2. UNIQUE - Ensures that all values in a column are different
    3. PRIMARY KEY - A combination of a NOT NULL and UNIQUE. Uniquely identifies each row in a table
    3. FOREIGN KEY - Prevents actions that would destroy links between tables
    4. CHECK - Ensures that the values in a column satisfies a specific condition
    5. DEFAULT - Sets a default value for a column if no value is specified
    6. CREATE INDEX - Used to create and retrieve data from the database very quickly
  -->
  <?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mysql_db";

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $sql = "CREATE TABLE MCu (
    id  INT(100) PRIMARY KEY,
    Marval VARCHAR(255) UNIQUE,
    Leage VARCHAR(255) NOT NULL,
    WW VARCHAR(255) UNIQUE)";
  $conn->exec($sql);
    echo "table and coloumn created ";
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;
?>