<!-- Math.cos(x) returns the cosine (a value between -1 and 1) of the angle x (given in radians)
    Angle in radians = Angle in degrees x PI / 180
 -->
 <html>
<body>

<h2>Math.cos()</h2>

<p id="demo"></p>

<script>
document.getElementById("demo").innerHTML = 
"The cosine value of 0 degrees is " + Math.cos(0 * Math.PI / 180);
</script>

</body>
</html>