<?php
    class Fruit {
        public $name;
        protected $color;
        private $weight;
    }

    $mango = new Fruit();
    $mango->name = 'banana'; // OK
    $mango->color = 'Yellow'; // ERROR
    $mango->weight = '100'; // ERROR
?>