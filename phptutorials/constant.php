<!-- syntax
define(name, value, case-insensitive) -->
<?php
// constant with case-sensetive
    define("GREET", "Hello World");
    echo GREET;
    echo "<br>";

// constant array
    define("color",["red", "green", "blue", "orange"]);
    echo color[0];
    echo "<br>";

// constant are global
    define("GREETING","hello world");

    function myTest() {
        echo GREETING;
    }
    myTest();
    echo "<br>";
?>