<!-- Overriding Inherited Methods -->
<?php
class car {
  public $name;
  public $color;
  public function __construct($name, $color) {
    $this->name = $name;
    $this->color = $color;
    }
    public function intro() {
        echo "Car is {$this->name} and the color is {$this->color}.";
    }
}
class bmw extends car {
    public $cylenders;
    public function __construct($name,$color,$cylenders){
        $this->name = $name;
        $this->color  = $color;
        $this->cylenders = $cylenders;
    }
    public function intro() {
        echo "Car is {$this->name} the color is {$this->color} and amout of cylenders {$this->cylenders}";
    }
}
$bmw = new bmw("bmws6","red",5);
$bmw->intro();
?>