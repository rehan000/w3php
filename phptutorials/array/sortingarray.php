<!-- Sort Functions For Arrays -->
<?php
// sort() - sort arrays in ascending order
    $cars = array("Volvo", "BMW", "Toyota");
    sort($cars);

    $clength = count($cars);
    for($x = 0; $x < $clength; $x++) {
        echo $cars[$x];
        echo "<br>";
    }

    echo "<br>";

    $num = array(10,2,13,6,19,7,4);
    sort($num);

    $arrlenght = count($num);
    for($x = 0; $x < $arrlenght; $x++){
        echo $num[$x];
        echo "<br>";
    }

    echo "<br>";

// rsort() - sort arrays in descending order
    $cars = array("Volvo", "BMW", "Toyota");
    rsort($cars);

    $clength = count($cars);
    for($x = 0; $x < $clength; $x++) {
        echo $cars[$x];
        echo "<br>";
    }

    echo "<br>";

    $num = array(10,2,13,6,19,7,4);
    rsort($num);

    $arrlenght = count($num);
    for($x = 0; $x < $arrlenght; $x++){
        echo $num[$x];
        echo "<br>";
    }

    echo "<br>";

    // asort() - sort associative arrays in ascending order, according to the value

    $age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");
    asort($age);

    foreach($age as $x => $x_value) {
        echo "Key=" . $x . ", Value=" . $x_value;
        echo "<br>";
    }

    echo "<br>";

    // ksort() - sort associative arrays in ascending order, according to the key
    $age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");
    ksort($age);

    foreach($age as $x => $x_value) {
        echo "Key=" . $x . ", Value=" . $x_value;
        echo "<br>";
    }

    echo "<br>";

    // arsort() - sort associative arrays in descending order, according to the value

    $age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");
    arsort($age);

    foreach($age as $x => $x_value) {
        echo "Key=" . $x . ", Value=" . $x_value;
        echo "<br>";
    }

    echo "<br>";

    // krsort() - sort associative arrays in descending order, according to the key

    $age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");
    krsort($age);

    foreach($age as $x => $x_value) {
        echo "Key=" . $x . ", Value=" . $x_value;
        echo "<br>";
    }

    echo "<br>";
?>