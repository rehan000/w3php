<!-- accessing Object Properties
    way1.
 -->
<html>
<body>

<h2>js objects</h2>

<p id="demo"></p>

<script>
var person = {
  firstName: "rehan",
  lastName : "fazal",
  id     :  5566
};
document.getElementById("demo").innerHTML =
person.firstName + " " + person.lastName;
</script>

</body>
</html>