<html>
<body>

<p>Convert string to upper case:</p>

<button onclick="myFunction()">click</button>

<p id="demo">Hello World</p>

<script>
function myFunction() {         //string is converted to upper case with toUpperCase()
  var text = document.getElementById("demo").innerHTML;
  document.getElementById("demo").innerHTML = text.toUpperCase();
}
</script>

</body>
</html>