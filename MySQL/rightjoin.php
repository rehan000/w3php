<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mysql_db";
try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $stmt = $conn->prepare("SELECT orders.id, employes.elname, employes.ename
  FROM orders
  RIGHT JOIN employes ON orders.id = employes.id
  ORDER BY orders.id");
  $stmt->execute();
  $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
  print_r($result);
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;
?>