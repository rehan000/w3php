<!-- JavaScript is one of the 3 languages all web developers must learn:

   1. HTML to define the content of web pages
   2. CSS to specify the layout of web pages
   3. JavaScript to program the behavior of web pages
   4. it has unique id
 -->

<!-- Placing scripts in external files has some advantages:
   It separates HTML and code
   It makes HTML and JavaScript easier to read and maintain
   Cached JavaScript files can speed up page loads 
-->

<!-- JavaScript can "display" data in different ways:

   Writing into an HTML element, using innerHTML.
   Writing into the HTML output using document.write().
   Writing into an alert box, using window.alert().
   Writing into the browser console, using console.log()
 -->
