<!-- Syntax
ALTER TABLE table_name
MODIFY COLUMN column_name datatype;
for modify datatype -->

<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "join";

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $sql = "ALTER TABLE customers
  MODIFY COLUMN postalcode INT(100)";
  $conn->exec($sql);
    echo "coloum modified successfully";
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;
?>