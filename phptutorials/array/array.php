<!-- An array stores multiple values in one single variable:
    2. Associative arrays - Arrays with named keys
    3. Multidimensional arrays - Arrays containing one or more arrays
 -->
<?php
    $cars = array("WV", "BMW", "BENZ");
    echo "My favrt " . $cars[0] . ", " . $cars[1] . " and " . $cars[2] . ".";

    echo "<br>";
// The count() Function

    $color = array("red", "blue", "green");
    echo count($color);
?>