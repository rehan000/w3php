<!-- string are writtent between single or dubble quotes -->
<html>
<body>

<h2>JavaScript Strings</h2>
<p id="demo"></p>

<script>
var fruit1 = "banana";
var fruit2 = 'banana';

document.getElementById("demo").innerHTML =
fruit1 + "<br>" + 
fruit2; 
</script>

</body>
</html>