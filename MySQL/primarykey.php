<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mysql_db";

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $sql = "CREATE TABLE Gangs (
    id int NOT NULL,
    Gangname varchar(255) NOT NULL,
    area varchar(255),
    persons int,
    PRIMARY KEY (id)
)";
  $conn->exec($sql);
    echo "table created with primary-key constraints";
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;
?>