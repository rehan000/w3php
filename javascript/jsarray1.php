<!-- Spaces and line breaks are not important. A declaration can span multiple lines -->
<html>
<body>
<p id="demo"></p>

<script>
var cars = [
  "benz",
  "ferrari",
  "BMW"
];
document.getElementById("demo").innerHTML = cars;
</script>

</body>
</html>