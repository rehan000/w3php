<html>

<head>
    <style>
    table,
    th,
    td {
        border: 1px solid black;
        border-collapse: collapse;
    }

    th,
    td {
        padding: 5px;
    }
    </style>
</head>

<body>
    <table>
        <tr>
            <td>Filter Name</td>
            <td>Filter ID</td>
        </tr>
        <?php
        /** 
         * filter_var() -> validate and senatize
         * filter_var() function filters a single variable with a specified filter. It takes two pieces of data:
         * 1. The variable you want to check
         * 2. The type of check to use
         * **/
            foreach (filter_list() as $id => $filter) {
                echo "<tr><td>" . $filter . "</td><td>" . filter_id($filter) . "</td></tr>";
            }
        ?>
    </table>
</body>

</html>